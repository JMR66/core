<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c"  %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix = "fn"  uri = "http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>   
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="author" content="Core Netowkrs Sevilla">
<meta name="description" content="Blog creado con Spring Boot, Spring MVC, JPA e Hibernate">

<title>CoreQA</title>

<link href="webjars/bootstrap/3.3.7-1/css/bootstrap.min.css"  rel="stylesheet">
<link href="assets/css/profile.css"  rel="stylesheet">
</head>
<body>
  <div class="container">
    <div class="header clearfix">
	    <nav>
	     <ul  class="nav nav-pills pull-right">
		       <c:choose>
		         <c:when test="${not empty sessionScope.userLoggedIn }">
		            <jsp:include page="includes/menu_logged.jsp">
		            <jsp:param value="profesores" name="profesores"/>
		            <jsp:param name="usuario" value="${sessionScope.userLoggedIn.nombre }"/>
		            
		          </jsp:include>
		         </c:when>
		         <c:otherwise>
		          <jsp:include page="includes/menu.jsp">
		            <jsp:param value="profesores" name="profesores"/>
		          </jsp:include>
		         </c:otherwise> 
		       </c:choose>   
        </ul>
	    </nav>
	    <h3 class="text-muted">CoreQA</h3>
    
    </div>
    <c:forEach items="${profesores}" var="profesor">
      <div class="row">
       <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10 col-xs-offset-0 col-sm-offset-0  col-md-offset-1 col-lg-offset-1 toppad">
          <div class="panel panel-info">
	            <div class="panel-heading">
	               <h3 class="panel.title"> ${profesor.nombre}  </h3>
	            </div>     
	            <div class="panel-body">
	               <div class="col-md-3 col-lg-3" align="center">
	                  	<img alt="Foto del Usuario" src="http://i.pravatar.cc/150?u=${profesor.email}"
	                      		class="img-circle img-responsive" />
	               </div>
	               <div class="col-md-9 col-lg-9">
	                  <table class="table table-user-information">
	                    <tbody>
	                      <tr>
	                         <td>Fecha de alta</td>
	                         <td>
	                            <fmt:formatDate  pattern="dd/MM/yyyy"  value="${profesor.fechaAlta}"/>
	                            </td>
	                      
	                      </tr>
	                      <tr>
	                         <td>Ciudad </td>
	                         <td>${profesor.ciudad}</td>	                      
	                      </tr>
	                      <tr>
	                         <td>Email </td>
	                         <td>${profesor.email}</td>	                      
	                      </tr>
	                      <tr>
	                         <td>Actividad </td>
	                         <td>Pendiente uno a muchos en Comentarios y Posts</td>	                      
	                      </tr>
	                      
	                    
	                    
	                    </tbody>
	                  
	                  
	                  </table>
	               
	               </div>
	            
	            </div>
	            <div class="panel-footer">
	            <p></p>
	            </div>

    
          </div>
       
       
       
       </div>
      
     
      </div>     
    
    </c:forEach>
    
    <footer class="footer">
      <p></p>
    
    </footer>
    
  
  
  </div><!--  Fin de container -->
  
  
  <script src="webjars/jquery/3.1.1/jquery.min.js"></script> 
  <script src="webjars/bootstrap/3.3.7-1/js/bootstrap.min.js"></script>
  

  
</body>
</html>