package com.corenetworks.hibernate.blog.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.corenetworks.hibernate.blog.model.Pregunta;
import com.corenetworks.hibernate.blog.model.Profesor;

@Repository
@Transactional
public class PreguntaDao {
	
    @PersistenceContext
	private EntityManager entityManager;
    /*
     * Almacena el post en la base de datos
     */
    public void create(Pregunta pregunta) {
    	entityManager.persist(pregunta);
    	return;
    }
    
    @SuppressWarnings("unchecked")
	public List<Pregunta> getAll(){
    	return entityManager
    			.createQuery("select p from Pregunta p")
    			.getResultList();
    }
    
    
 	/**
	 * Devuelve un post en base a su Id
	 */
	public Pregunta getById(long id) {
		return entityManager.find(Pregunta.class, id);
	}

    
    
    
}
