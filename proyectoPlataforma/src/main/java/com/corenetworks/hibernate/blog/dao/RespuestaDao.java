package com.corenetworks.hibernate.blog.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.corenetworks.hibernate.blog.model.Respuesta;


@Repository
@Transactional
public class RespuestaDao {
	
    @PersistenceContext
	private EntityManager entityManager;
    /*
     * Almacena el Comment en la base de datos
     */
    public void create(Respuesta respuesta) {
    	entityManager.persist(respuesta);
    	return;
    }
    
    @SuppressWarnings("unchecked")
	public List<Respuesta> getAll(){
    	return entityManager
    			.createQuery("select c from Respuesta c")
    			.getResultList();
    }
    
    
 	/**
	 * Devuelve un post en base a su Id
	 */
	public Respuesta getById(long id) {
		return entityManager.find(Respuesta.class, id);
	}

    
    
    
}
