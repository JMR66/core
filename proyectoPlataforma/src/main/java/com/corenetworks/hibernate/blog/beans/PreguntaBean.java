package com.corenetworks.hibernate.blog.beans;

public class PreguntaBean {
	
	private String url;
	
	private String titulo;
	
	private String contenido;
	
	public PreguntaBean() {
		
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getContenido() {
		return contenido;
	}

	public void setContenido(String contenido) {
		this.contenido = contenido;
	}
	
	

}
